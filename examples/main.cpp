#include <timber/log/backends/stdout/backend.hpp>

#include <persist/matrix/fs/fs.hpp>
#include <persist/matrix/db/database.hpp>

#include <iostream>

void PrintSnapshot(persist::db::ISnapshotPtr snapshot) {
  auto iter = snapshot->MakeIterator();
  while (iter->IsValid()) {
    std::cout << iter->Key() << " -> " << iter->Value() << std::endl;
    iter->MoveNext();
  }
}

int main() {
  // Example runtime
  timber::backends::StdoutLogBackend log_backend;
  persist::fs::matrix::FileSystem fs{log_backend};
  persist::db::matrix::Database db{fs, log_backend};

  // Example

  {
    db.Open("/db");

    db.Put("k1", "v1");

    auto value = db.TryGet("k1");
    std::cout << "k1 ->" << *value << std::endl;

    db.Put("k2", "v2");

    auto snapshot1 = db.MakeSnapshot();

    db.Put("k1", "v3");

    {
      std::cout << "Snapshot #1" << std::endl;
      PrintSnapshot(snapshot1);
    }

    {
      auto snapshot2 = db.MakeSnapshot();
      std::cout << "Snapshot #2" << std::endl;
      PrintSnapshot(snapshot2);
    }

    {
      auto writes = db.MakeBatchWriter();
      writes->Put("k3", "v5");
      writes->Delete("k1");

      writes->Write();
    }

    {
      auto snapshot3 = db.MakeSnapshot();
      std::cout << "Snapshot #3" << std::endl;
      PrintSnapshot(snapshot3);
    }
  }

  return 0;
}
