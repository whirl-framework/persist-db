#pragma once

#include <persist/db/kv.hpp>

#include <memory>

namespace persist::db {

struct IIterator {
  virtual ~IIterator() = default;

  virtual bool IsValid() const = 0;

  // Current entry
  virtual KeyView Key() const = 0;
  virtual ValueView Value() const = 0;

  // Position at the first key that is at or past `target`
  // ~ lower_bound
  virtual void Seek(const db::Key& target) = 0;

  virtual void SeekToLast() = 0;
  virtual void SeekToFirst() = 0;

  // Move to the next entry
  virtual void MoveNext() = 0;
  // Move to the previous entry
  virtual void MovePrev() = 0;
};

using IIteratorPtr = std::shared_ptr<IIterator>;

}  // namespace persist::db
