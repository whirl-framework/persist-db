#pragma once

#include <persist/db/kv.hpp>
#include <persist/db/comparator.hpp>
#include <persist/db/snapshot.hpp>
#include <persist/db/write_batch.hpp>

#include <optional>

namespace persist::db {

// Persistent ordered mapping from string keys to string values
// with lightweight snapshots and batch atomic writes

// ~ LevelDB (https://github.com/google/leveldb)

struct IDatabase {
  virtual ~IDatabase() = default;

  // With custom key comparator
  virtual void Open(const std::string& work_dir,
                    IKeyComparatorPtr comparator) = 0;

  // With default less-than key comparator
  virtual void Open(const std::string& work_dir) = 0;

  // All operations are synchronous!

  // Single-key operations
  virtual void Put(const Key& key, const Value& value) = 0;
  virtual std::optional<Value> TryGet(const Key& key) const = 0;
  virtual void Delete(const Key& key) = 0;

  // Multi-key atomic write
  virtual IBatchWriterPtr MakeBatchWriter() = 0;

  // Lightweight immutable consistent snapshot
  virtual ISnapshotPtr MakeSnapshot() = 0;
};

}  // namespace persist::db
