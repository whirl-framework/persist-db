#pragma once

#include <persist/db/iterator.hpp>

#include <memory>
#include <optional>

namespace persist::db {

// Immutable consistent snapshot

struct ISnapshot {
  virtual ~ISnapshot() = default;

  virtual std::optional<Value> TryGet(const Key& key) const = 0;

  virtual IIteratorPtr MakeIterator() = 0;
};

using ISnapshotPtr = std::shared_ptr<ISnapshot>;

}  // namespace persist::db
