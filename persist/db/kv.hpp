#pragma once

#include <string>

namespace persist::db {

using Key = std::string;
using Value = std::string;

// Non-owning view
using KeyView = std::string_view;
using ValueView = std::string_view;

}  // namespace persist::db
