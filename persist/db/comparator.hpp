#pragma once

#include <persist/db/kv.hpp>

#include <memory>

namespace persist::db {

struct IKeyComparator {
  virtual ~IKeyComparator() = default;

  // Three-way comparison:
  //   if lhs < rhs: -1
  //   if lhs > rhs:  1
  //   else: 0
  virtual int Compare(KeyView lhs, const KeyView rhs) const = 0;
};

using IKeyComparatorPtr = std::shared_ptr<IKeyComparator>;

}  // namespace persist::db
