#pragma once

#include <persist/db/kv.hpp>

#include <memory>

namespace persist::db {

struct IBatchWriter {
  virtual ~IBatchWriter() = default;

  // Add mutations to this write batch
  virtual void Put(Key key, Value value) = 0;
  virtual void Delete(Key key) = 0;

  // Write all accumulated mutations to the database
  // as a single atomic batch
  // One-shot!
  virtual void Write() = 0;
};

using IBatchWriterPtr = std::unique_ptr<IBatchWriter>;

}  // namespace persist::db
