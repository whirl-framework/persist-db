#pragma once

#include <persist/matrix/db/entries.hpp>

#include <optional>

namespace persist::db::matrix {

// Sorted in-memory key -> value mapping

class MemTable {
 public:
  explicit MemTable(IKeyComparatorPtr comparator)
    : entries_({comparator}) {
  }

  void Put(persist::db::Key key, persist::db::Value value) {
    entries_.insert_or_assign(key, value);
  }

  std::optional<persist::db::Value> TryGet(persist::db::Key key) const {
    auto it = entries_.find(key);
    if (it != entries_.end()) {
      return it->second;
    } else {
      return std::nullopt;
    }
  }

  void Delete(persist::db::Key key) {
    entries_.erase(key);
  }

  void Clear() {
    entries_.clear();
  }

  const Entries& GetEntries() const {
    return entries_;
  }

 private:
  Entries entries_;
};

}  // namespace persist::db::matrix
