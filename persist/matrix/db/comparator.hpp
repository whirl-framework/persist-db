#pragma once

#include <persist/db/comparator.hpp>

namespace persist::db::matrix {

//////////////////////////////////////////////////////////////////////

struct LessKeyComparator : IKeyComparator {
  int Compare(KeyView lhs, KeyView rhs) const override {
    if (lhs == rhs) {
      return 0;
    }
    return (lhs < rhs) ? -1 : 1;
  }
};

//////////////////////////////////////////////////////////////////////

inline IKeyComparatorPtr MakeDefaultComparator() {
  return std::make_shared<LessKeyComparator>();
}

}  // namespace persist::db::matrix
