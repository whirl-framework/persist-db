#pragma once

#include <persist/db/database.hpp>

#include <persist/fs/fs.hpp>

#include <timber/log/backend.hpp>

#include <persist/matrix/db/impl.hpp>

namespace persist::db::matrix {

// Implemented in userspace

class Database : public persist::db::IDatabase {
  friend class Iterator;
  friend class BatchWriter;

  using WriteBatch = proto::persist::db::matrix::WriteBatch;

 public:
  Database(persist::fs::IFileSystem& fs, timber::ILogBackend& log)
    : fs_(fs), log_(log) {
  }

  // With user-provided key comparator
  void Open(const std::string& work_dir, IKeyComparatorPtr comparator) override;

  // With default less-than key comparator
  void Open(const std::string& work_dir) override;

  // Mutate

  void Put(const persist::db::Key& key, const persist::db::Value& value) override;
  void Delete(const persist::db::Key& key) override;

  IBatchWriterPtr MakeBatchWriter() override;

  // Read

  std::optional<persist::db::Value> TryGet(
      const persist::db::Key& key) const override;

  persist::db::ISnapshotPtr MakeSnapshot() override;

 private:
  class Impl& Impl() const;

 private:
  persist::fs::IFileSystem& fs_;
  timber::ILogBackend& log_;

  mutable std::optional<class Impl> impl_;
};

}  // namespace persist::db::matrix
