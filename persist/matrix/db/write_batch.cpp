#include <persist/matrix/db/write_batch.hpp>

#include <persist/matrix/db/impl.hpp>

namespace persist::db::matrix {

void BatchWriter::Put(db::Key key, db::Value value) {
  auto* mut = writes_.add_muts();
  mut->set_put(true);
  mut->set_key(std::move(key));
  mut->set_value(std::move(value));
}

void BatchWriter::Delete(db::Key key) {
  auto* mut = writes_.add_muts();
  mut->set_put(false);
  mut->set_key(std::move(key));
}

void BatchWriter::Write() {
  db_->Write(writes_);
  writes_.Clear();
}

}  // namespace persist::db::matrix
