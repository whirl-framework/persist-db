#include <persist/matrix/db/impl.hpp>

#include <persist/matrix/db/comparator.hpp>
#include <persist/matrix/db/snapshot.hpp>
#include <persist/matrix/db/write_batch.hpp>

#include <timber/log/log.hpp>

#include <wheels/memory/view_of.hpp>
#include <wheels/support/assert.hpp>

using persist::db::Key;
using persist::db::Value;

namespace persist::db::matrix {

Impl::Impl(persist::fs::Path work_dir,
     persist::db::IKeyComparatorPtr comparator,
     timber::ILogBackend& log)
    : work_dir_(work_dir),
      fs_(work_dir.Fs()),
      mem_table_(comparator),
      wal_(LogPath()),
      logger_("Database", log) {
}

void Impl::Load() {
  wal_.Open(ReplayWAL(LogPath()));
  PrepareSSTable();
}

void Impl::Put(const Key& key, const Value& value) {
  //TIMBER_LOG_DEBUG("Put('{}', '{}')", key, log::FormatMessage(value));

  WriteBatch batch;

  auto* mut = batch.add_muts();
  mut->set_put(true);
  mut->set_key(key);
  mut->set_value(value);

  Write(batch);
}

void Impl::Delete(const Key& key) {
  //TIMBER_LOG_DEBUG("Delete('{}')", key);

  WriteBatch batch;

  auto* mut = batch.add_muts();
  mut->set_put(false);
  mut->set_key(key);

  Write(batch);
}

bool Impl::CacheMiss() const {
  return false;
}

bool Impl::IteratorSeek() const {
  return false;
}

std::optional<Value> Impl::TryGet(const Key& key) const {
  TIMBER_LOG_DEBUG("TryGet({})", key);

  if (CacheMiss()) {
    AccessSSTable();
  }

  return mem_table_.TryGet(key);
}

persist::db::IBatchWriterPtr Impl::MakeBatchWriter() {
  return std::make_unique<BatchWriter>(this);
}

void Impl::IteratorMove() {
  if (IteratorSeek()) {
    AccessSSTable();
  }
}

persist::db::ISnapshotPtr Impl::MakeSnapshot() {
  TIMBER_LOG_DEBUG("Make snapshot at version {}", version_);
  return std::make_shared<Snapshot>(this, mem_table_.GetEntries(), version_);
}

void Impl::Write(const WriteBatch& batch) {
  TIMBER_LOG_DEBUG("Write {} mutations as atomic batch", batch.muts().size());

  {
    std::lock_guard locker(write_mutex_);

    wal_.Append(batch);
    ApplyToMemTable(batch);
    ++version_;
  }
}

void Impl::ApplyToMemTable(const WriteBatch& batch) {
  for (const auto& mut : batch.muts()) {
    if (mut.put()) {
      TIMBER_LOG_DEBUG("Apply Put('{}', '{} bytes') to MemTable", mut.key(), mut.value().size());
      mem_table_.Put(mut.key(), mut.value());
    } else {
      TIMBER_LOG_DEBUG("Apply Delete('{}') to MemTable", mut.key());
      mem_table_.Delete(mut.key());
    }
  }
}

size_t Impl::ReplayWAL(persist::fs::Path wal_path) {
  mem_table_.Clear();

  TIMBER_LOG_DEBUG("Replaying WAL -> MemTable");

  if (!fs_.Exists(wal_path)) {
    return 0;
  }

  version_ = 0;

  WALReader wal_reader(wal_path);

  while (auto batch = wal_reader.ReadNext()) {
    ApplyToMemTable(*batch);
    ++version_;
  }

  TIMBER_LOG_DEBUG("MemTable populated");

  return wal_reader.WriterOffset();
}

// Emulate read latency

void Impl::PrepareSSTable() {
  auto sstable_path = SSTablePath();
  if (!fs_.Exists(sstable_path)) {
    fs_.Create(sstable_path).ExpectOk();

    persist::fs::FileWriter writer(sstable_path);
    writer.Open().ExpectOk();
    writer.Write(wheels::ViewOf("data")).ExpectOk();
  }
}

void Impl::AccessSSTable() const {
  TIMBER_LOG_DEBUG("Cache miss, access SSTable on disk");

  persist::fs::FileReader reader(SSTablePath());
  reader.Open().ExpectOk();

  char buf[128];
  reader.ReadSome(wheels::MutViewOf(buf)).ExpectOk();
}

}  // namespace persist::db::matrix
