#pragma once

#include <persist/matrix/db/write_batch.hpp>

#include <persist/fs/fs.hpp>
#include <persist/log/reader.hpp>
#include <persist/log/writer.hpp>

namespace persist::db::matrix {

// Write ahead log (WAL) writer / reader

//////////////////////////////////////////////////////////////////////

class WALWriter {
 public:
  WALWriter(persist::fs::Path file_path)
      : log_writer_(file_path) {
  }

  void Open(size_t offset) {
    log_writer_.Open(offset).ExpectOk();
  }

  // Atomic
  void Append(const WriteBatch& batch);

 private:
  persist::log::LogWriter log_writer_;
};

//////////////////////////////////////////////////////////////////////

class WALReader {
 public:
  WALReader(persist::fs::Path file_path)
      : log_reader_(file_path) {
  }

  std::optional<WriteBatch> ReadNext();

  size_t WriterOffset() const {
    return log_reader_.OffsetForWriter();
  }

 private:
  persist::log::LogReader log_reader_;
};

}  // namespace persist::db::matrix
