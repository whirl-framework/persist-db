#pragma once

#include <persist/db/iterator.hpp>
#include <persist/db/snapshot.hpp>

#include <persist/matrix/db/entries.hpp>
#include <persist/matrix/db/snapshot.hpp>

namespace persist::db::matrix {

class Iterator : public persist::db::IIterator {
 public:
  explicit Iterator(SnapshotPtr snapshot);

  // IIterator

  persist::db::KeyView Key() const override;
  persist::db::ValueView Value() const override;

  void SeekToFirst() override;
  void SeekToLast() override;
  void Seek(const persist::db::Key& target) override;

  bool IsValid() const override;

  void MoveNext() override;
  void MovePrev() override;

 private:
  void EnsureValid() const;

  // Access new key
  void Move();

 private:
  SnapshotPtr snapshot_;

  const Entries& entries_;

  bool valid_ = false;
  Entries::const_iterator it_;
};

}  // namespace persist::db::matrix
