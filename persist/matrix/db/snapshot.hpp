#pragma once

#include <persist/db/snapshot.hpp>

#include <persist/matrix/db/fwd.hpp>
#include <persist/matrix/db/entries.hpp>

namespace persist::db::matrix {

class Snapshot : public persist::db::ISnapshot,
                 public std::enable_shared_from_this<Snapshot> {
 public:
  Snapshot(Impl* db, Entries entries, uint64_t version)
      : db_(db),
        entries_(std::move(entries))
      , version_(version) {
  }

  // ISnapshot

  std::optional<persist::db::Value> TryGet(
      const persist::db::Key& key) const override;

  persist::db::IIteratorPtr MakeIterator() override;

  // Access

  const Entries& GetEntries() const {
    return entries_;
  }

  uint64_t Version() const {
    return version_;
  }

  Impl* Db() {
    return db_;
  }

 private:
  Impl* db_;
  const Entries entries_;
  uint64_t version_;
};

using SnapshotPtr = std::shared_ptr<Snapshot>;

}  // namespace persist::db::matrix
