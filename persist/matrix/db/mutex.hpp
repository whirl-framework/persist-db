#pragma once

namespace persist::db::matrix {

class Mutex {
 public:
  void lock() {
    // No-op
  }

  void unlock() {
    // No-op
  }
};

}  // namespace persist::db::matrix
