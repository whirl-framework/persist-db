#pragma once

#include <persist/db/kv.hpp>
#include <persist/db/comparator.hpp>

#include <map>

namespace persist::db::matrix {

//////////////////////////////////////////////////////////////////////

class EntryComparator {
 public:
  EntryComparator(IKeyComparatorPtr impl) : impl_(impl) {
  }

  bool operator()(const db::Key& lhs, const db::Key& rhs) const {
    return impl_->Compare(lhs, rhs) < 0;
  }
 private:
  db::IKeyComparatorPtr impl_;
};

//////////////////////////////////////////////////////////////////////

using Entries = std::map<db::Key, db::Value, EntryComparator>;

}  // persist::db::matrix
