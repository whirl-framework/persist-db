#pragma once

#include <persist/db/database.hpp>
#include <persist/fs/fs.hpp>

#include <persist/matrix/db/mem_table.hpp>
#include <persist/matrix/db/wal.hpp>
#include <persist/matrix/db/mutex.hpp>

#include "proto/cpp/wal.pb.h"

#include <timber/log/logger.hpp>

namespace persist::db::matrix {

// Implemented in userspace

class Impl {
  friend class Iterator;
  friend class BatchWriter;

 public:
  Impl(persist::fs::Path work_dir,
       persist::db::IKeyComparatorPtr comparator,
       timber::ILogBackend& log);

  void Load();

  // Mutate

  void Put(const persist::db::Key& key, const persist::db::Value& value);
  void Delete(const persist::db::Key& key);

  // Batch write

  persist::db::IBatchWriterPtr MakeBatchWriter();

  // Read

  std::optional<persist::db::Value> TryGet(
      const persist::db::Key& key) const;

  persist::db::ISnapshotPtr MakeSnapshot();

 private:
  void Write(const WriteBatch& batch);
  void ApplyToMemTable(const WriteBatch& batch);

  // Returns start offset for log writer
  size_t ReplayWAL(persist::fs::Path wal_path);

  void IteratorMove();

  persist::fs::Path LogPath() const {
    return work_dir_ / "wal";
  }

  // Emulate read latency

  persist::fs::Path SSTablePath() const {
    return work_dir_ / "sstable";
  }

  bool CacheMiss() const;
  bool IteratorSeek() const;

  void AccessSSTable() const;
  void PrepareSSTable();

 private:
  persist::fs::Path work_dir_;
  persist::fs::IFileSystem& fs_;

  persist::db::IKeyComparatorPtr comparator_;

  MemTable mem_table_;
  WALWriter wal_;
  Mutex write_mutex_;

  // Incremented on each (batch) mutation
  uint64_t version_ = 0;

  mutable timber::Logger logger_;
};

}  // namespace persist::db::matrix
