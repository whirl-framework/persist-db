#pragma once

#include <persist/db/write_batch.hpp>

#include <persist/matrix/db/fwd.hpp>

#include "proto/cpp/wal.pb.h"

namespace persist::db::matrix {

//////////////////////////////////////////////////////////////////////

using WriteBatch = proto::persist::db::matrix::WriteBatch;

//////////////////////////////////////////////////////////////////////

class BatchWriter : public IBatchWriter {
 public:
  BatchWriter(Impl* db) : db_(db) {
  }

  // Add mutations
  void Put(db::Key key, db::Value value) override;
  void Delete(db::Key key) override;

  // Do actual write
  void Write() override;

 private:
  Impl* db_;
  WriteBatch writes_;
};

}  // namespace persist::db::matrix
