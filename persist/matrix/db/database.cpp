#include <persist/matrix/db/database.hpp>

#include <persist/matrix/db/comparator.hpp>

using persist::db::Key;
using persist::db::Value;

namespace persist::db::matrix {

void Database::Open(const std::string& work_dir,
                    persist::db::IKeyComparatorPtr comparator) {
  impl_.emplace(fs_.MakePath(work_dir), comparator, log_);
  impl_->Load();
}

void Database::Open(const std::string& work_dir) {
  Open(work_dir, MakeDefaultComparator());
}

void Database::Put(const Key& key, const Value& value) {
  Impl().Put(key, value);
}

void Database::Delete(const Key& key) {
  Impl().Delete(key);
}

std::optional<Value> Database::TryGet(const Key& key) const {
  return Impl().TryGet(key);
}

persist::db::IBatchWriterPtr Database::MakeBatchWriter() {
  return Impl().MakeBatchWriter();
}

persist::db::ISnapshotPtr Database::MakeSnapshot() {
  return Impl().MakeSnapshot();
}

class Impl& Database::Impl() const {
  WHEELS_VERIFY(impl_.has_value(), "Open database before first use!");
  return *impl_;
}

}  // namespace persist::db::matrix
