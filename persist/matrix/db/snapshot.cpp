#include <persist/matrix/db/snapshot.hpp>

#include <persist/matrix/db/iterator.hpp>

namespace persist::db::matrix {

std::optional<persist::db::Value> Snapshot::TryGet(
    const persist::db::Key& key) const {
  auto it = entries_.find(key);
  if (it != entries_.end()) {
    return it->second;
  } else {
    return std::nullopt;
  }
}

persist::db::IIteratorPtr Snapshot::MakeIterator() {
  auto self = shared_from_this();
  return std::make_shared<Iterator>(self);
}

}  // namespace persist::db::matrix
