#include <persist/matrix/db/wal.hpp>

#include <pickle/serialize.hpp>

#include <wheels/memory/view_of.hpp>

namespace persist::db::matrix {

void WALWriter::Append(const WriteBatch& batch) {
  auto bytes = pickle::Serialize(batch);
  log_writer_.Append(wheels::ViewOf(bytes)).ExpectOk();
}

std::optional<WriteBatch> WALReader::ReadNext() {
  auto record = log_reader_.ReadNext();
  if (!record.has_value()) {
    return std::nullopt;
  }

  WriteBatch batch;
  if (pickle::Deserialize(*record, &batch)) {
    return std::move(batch);
  } else {
    return std::nullopt;
  }
}

}  // namespace persist::db::matrix
